$(document).ready(function() {
    let aulas = DevuelveAulasUnicas(equips);
    let datas = DevuelveDatas(equips);
    let rams = devuelveMacYRam(equips);

    //Variables charts

    //horizontal

    let macsYAulas = devuelveMacsYAulas(equips)
    let macsXAula = ordenadoresXAula(aulas, macsYAulas)


    //pie
    let tiposRam = devuelveRamsUnicos(rams);
    let cuentaRams = cuentaCuantosConRam(rams, tiposRam)

    //let macs = devuelveMacs(equips);



    let fromAnterior = 0;
    let untilAnterior = 0;


    //let connXRam= devuelveConnectionesXRam(tiposRam)
    //console.log(tiposRam)
    //Pintamos Aulas
    pintaAulas(aulas)


    $(".divHeader").on("click", "#classPcs", function() {
        console.log("hola")
        pintaOrdenadoresClicked(equips, clicked)
    })

    /**********Cards*************/
    //Variables Cards
    let nMaquines = macsYAulas.length;
    let ultimoMes = last30days(datas);
    let ultimaConection = lastConnection(datas);

    $("#nMaquines").html(nMaquines)
    $("#monthlyConnections").html(ultimoMes)
    $("#lastConnection").html(ultimaConection)
    PintaRamCard(equips);
    $(".cards").fadeIn()

    /**********Events************/

    $("#listaConnectiones").on("click", "li", function name(params) {
        let count = 0;
        let mac = $(this).children(".mac").text()
        console.log(mac)
        pintaModalMac(equips, mac)
    })
    $("#listaConnectiones").on("click", ".btn", function name(params) {
        let mac = $(this).text()
        console.log($(this).text())
        pintaModalMac(equips, mac)
    })

    $('#listaAulas').on('click', '.aulas', function() {

        clicked = $(this).text();
        pintaClicked(equips, clicked)
        $(".connectionesAula").fadeIn()
        $("#listaConnectiones").fadeIn();
        //  $(".total").html("<h5>" + count + "</h5>")
    })
    $('#aulaYConnectiones').on('click', '#connections', function() {

        pintaClicked(equips, clicked)
        $(".connectionesAula").fadeIn()
        $("#listaConnectiones").fadeIn();
        //  $(".total").html("<h5>" + count + "</h5>")
    })


    $('#listaAulas').on('click', '#all', function() {
        console.log("quiero ver todo")
        pintaTodo(equips)
    })
    $('.nav-item').on('click', function() {

        $('.nav-item').removeClass("active")
        $(this).addClass("active")
    })

    $("#dashboard").on("click", function() {
        hideAll()
        $(".landingLogo").fadeIn()
        $('.cards').fadeIn();

    })
    $('#botoTaules').on('click', function() {
        hideAll();
        $('#tables').fadeIn();
    })
    $("#classroomsButton").on("click", function() {
        hideAll()
        $('#divAulas').fadeIn();
        $(".connectionesAula").fadeOut()


    })
    $("#chartsButton").on('click', function() {
        hideAll()
        if ($('#charts').is(':hidden')) {
            $("#charts").show()

        } else {
            $("#charts").hide()

        }
    })

    $('#pieChartButton').on('click', function() {
        hideAll()
        $("#charts").show()
        if ($('#pie-chart').is(':hidden')) {
            $(".pies").show()

            $('#pie-chart').fadeIn();
        } else {
            $('#pie-chart').fadeOut()
            $(".pies").hide()

        }
    })
    $('#barChartButton').on('click', function() {
        $("#charts").show()

        if ($('#bar-chart').is(':hidden')) {
            $(".bars").show()
            $('#bar-chart').fadeIn();
        } else {
            $('#bar-chart').fadeOut()
            $(".bars").hide()
        }
    })
    $('#horizontalBarChartButton').on('click', function() {
        hideAll()
        $("#charts").show()

        console.log("hola")
        if ($('#bar-chart-horizontal').is(':hidden')) {
            $('#bar-chart-horizontal').show();
            $(".hbars").show()

        } else {
            $('#bar-chart-horizontal').fadeOut()
            $(".hbars").hide()

        }
    })

    $('#taulaCompleta').DataTable({
        data: equips,
        columns: [
            { data: 'data' },
            { data: 'mac' },
            { data: 'aula' },
            { data: 'ram' }
        ]
    });
    //Input Limit
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("fromLimit").setAttribute("max", today);




    /**********************     CHARTS      ***********************/
    // Bar chart
    let barChart = new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: aulas,
            datasets: [{
                label: "Connections",
                backgroundColor: ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
                ],

                data: ConnectionesXClase(equips, aulas)
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            },

            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Connections for each classroom'
            }
        }
    });
    // Pie Chart
    new Chart(document.getElementById("pie-chart"), {
        type: 'pie',
        data: {
            labels: tiposRam,
            datasets: [{
                label: "Population (millions)",
                backgroundColor: ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                    '#80B300', '#809900'
                ],
                data: cuentaRams
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Quantity of computers with ram type'
            }
        }
    });

    //Horizontal Bar Chart
    new Chart(document.getElementById("bar-chart-horizontal"), {
        type: 'horizontalBar',
        data: {
            labels: aulas,
            backgroundColor: ['#FF6633'],
            datasets: [{
                label: "Ordenadores por Aula ",
                data: macsXAula
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Computers in each classroom'
            }
        }
    });


    /* FROM Datepicker*/
    $("#fromLimit").datepicker({
        maxDate: new Date(),

        onSelect: function(dateText) {
            console.log(calculaDiferencia(dateText))
            document.getElementById("fromLimit").value = moment(dateText).format('MMMM Do YYYY');

            if (fromAnterior == 0) {
                fromAnterior = dateText;
                //console.log("ahora el FROMANTERIOR esta asignado")
            } else if (calculaDiferencia(dateText) > calculaDiferencia(fromAnterior)) {
                equips = DevuelveTodo()
                    //console.log("es Menor que el valor ant")
                fromAnterior = dateText;
            }

            equipoFiltrado.splice(0, equipoFiltrado.length)
            equips.forEach(function(element, i) {
                if (checkDateFrom(element.data, dateText)) {
                    equipoFiltrado.push(element)
                }
            });
            equips.splice(0, equips.length)
            equipoFiltrado.forEach(element => {
                equips.push(element)
            });

            console.log(equipoFiltrado)
            barChart.update()

            pintaAulasYconn(equips, clicked);
        }
    }).on("change", function() {

    });


    $("#reload").on("click", function(params) {
        $("#loadGid").fadeIn()
        console.log("reloading...")
        equips = DevuelveTodo()
        pintaAulasYconn(equips, clicked);

    })
    $('input[type=radio]').change(function() {
        let val = this.value
        equips = DevuelveTodo()
        equipoFiltrado.splice(0, equipoFiltrado.length)

        if (val === "Today") {
            equips.forEach(function(element, i) {
                if (hoy(element.data)) {
                    equipoFiltrado.push(element)
                }
            });
            equips.splice(0, equips.length)
            equipoFiltrado.forEach(element => {
                equips.push(element)
            });
            console.log(equips)
        } else if (val === "Yesterday") {
            console.log(val)
        } else if (val === "Last week") {
            console.log(val)
        } else if (val === "Last month") {
            console.log(val)
        }
    });
    /*UNTIL Datepicker */
    $("#untilLimit").datepicker({

        onSelect: function(dateText) {
            document.getElementById("untilLimit").value = moment(dateText).format('MMMM Do YYYY');
            equipoFiltrado.splice(0, equipoFiltrado.length)

            if (untilAnterior == 0) {
                untilAnterior = dateText;
                //console.log("ahora el untilANTERIOR esta asignado")
            } else if (calculaDiferencia(dateText) < calculaDiferencia(untilAnterior)) {
                equips = DevuelveTodo()
                    //console.log("es mayor que el valor ant")
                untilAnterior = dateText;
            }
            equips.forEach(function(element, i) {
                if (checkDateUntil(element.data, dateText)) {
                    equipoFiltrado.push(element)
                }
            });
            equips.splice(0, equips.length)
            equipoFiltrado.forEach(element => {
                equips.push(element)
            });

            console.log(equips)
            pintaAulasYconn(equips, clicked);
        }
    }).on("change", function() {
        display("Got change event from field");
    });

    barChart.update()
});