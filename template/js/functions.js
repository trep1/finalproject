let equips = DevuelveTodo();
let clicked;
var equipoFiltrado = []

console.log("TOTAL: " + equips.length + "hola")

function checkDateFrom(actual, picked) {
    var data = new Date(moment(actual))
    var dataInicio = new Date(moment(picked))
    if (data > dataInicio) return true;
    else return false;
}

function checkDateUntil(actual, picked) {
    var data = new Date(moment(actual))
    var dataInicio = new Date(moment(picked))
    if (dataInicio > data) return true;
    else return false;
}

function hoy(actual) {
    actual = moment(actual).format('L')
    actual = new Date(actual)

    var dataInicio = moment().format('L')
    dataInicio = new Date(dataInicio)
    if (dataInicio.getDate() === actual.getDate()) return true;
    else return false;
}

function filtra(equips, picked) {
    for (let i = 0; i < equips.length; i++) {
        let dataCon = new Date(moment(equips[i].data));

        if (dataCon.getDate() < picked.getDate()) {
            console.log("elimino elemento" + equips[i])
            equips.splice(i, 1);
        } else {
            ++i;
        }
    }
}

//Returns array with number of connections for each class. (1,2,3...)
function ConnectionesXClase(maquines, aulasUnicas) {
    let len = aulasUnicas.length
    let connAula = new Array(len).fill(0);
    for (let i = 0; i < maquines.length; i++) {
        for (let j = 0; j < aulasUnicas.length; j++) {
            if (maquines[i].aula === aulasUnicas[j]) {
                connAula[j]++
            }
        }
    }

    //console.log(connAula)
    return connAula;
}

function DevuelveAulasUnicas(maquines) {
    let unicas = []
    for (i = 0; i < maquines.length; i++) {
        //    console.log(maquines[i].aula)
        if (!(unicas.includes(maquines[i].aula))) {
            unicas.push(maquines[i].aula)
        }
    }
    unicas.sort()
    return unicas;
}

function DevuelveTodo() {
    let todo = []
    $.ajax({
            method: "GET",
            url: "http://labs.iam.cat/api/api.php/records/equips",
            dataType: 'json',

            async: false,
            beforeSend: function() {
                $(".load").fadeIn()
            }
        })
        .done(function(res) {
            let conection = res.records;
            conection.forEach(element => {
                todo.push(element)
            });
            $(".load").fadeOut()
                // $("#all_data").append(html)
        })
        .fail(function() {
            console.log("Error.")
        })
    return todo;
}

function DevuelveDatas(equips) {
    let datas = [];
    equips.forEach(element => {
        datas.push(element.data);
    });
    return datas;
}

function devuelveRams(equips) {
    let rams = [];
    equips.forEach(element => {
        rams.push(element.ram)
    });
    return rams;
}

function devuelveMinRam(equips) {
    let minRam = 100;
    equips.forEach(element => {
        if (parseFloat(element.ram) < parseFloat(minRam)) {
            minRam = element.ram
        }
    });
    return minRam;
}

function devuelveMaxRam(equips) {
    let maxRam = 0;
    equips.forEach(element => {
        if (parseFloat(element.ram) > parseFloat(maxRam)) {
            maxRam = element.ram
        }
    });
    return maxRam;
}

function pintaTodo(equips) {
    let html = ""
    let count = 0;

    equips.forEach(element => {
        let haceCuanto = element.data
        haceCuanto = moment(haceCuanto).format('YYYY-MM-DDTHH:mm:ssZ')
        count++;
        html += `<li class="conectionesAula ">
        <button class="mac btn btn-outline-primary" data-toggle="modal" data-target="#myModal">${element.mac}</button>
        ${element.nom}-<span class="fecha" data="${haceCuanto}"> ${moment(haceCuanto).fromNow()}<br>
        <small>${moment(element.data).format('YYYY-MM-DD')}</small></span></li>`
    });
    $("#aulaYConnectiones").html(`${clicked} <br><small id="classPcs">PC's:${pintaNumeroOrdenadoresClicked(equips,clicked)} </small>-<small> ${count} connections</small>`)
    $("#listaConnectiones").html(html);
}

function pintaAulas(aulasUnicas) {
    let html = "";
    $(".divTitle").html("Classrooms" + "(" + aulasUnicas.length + ")")
    aulasUnicas.forEach(element => {
        html += `<li class="collapse-item aulas">${element}</li>`
    });
    $("#listaAulas").html(html)
    $("#listaAulas").prepend(`<li id="all" class="collapse-item aulas">All</li>`)
}

function pintaClicked(equips, clicked) {

    let html = "";
    let count = 0;
    for (let i = 0; i < equips.length; i++) {
        if (equips[i].aula == clicked) {
            let haceCuanto = equips[i].data
            haceCuanto = moment(haceCuanto).format('YYYY-MM-DDTHH:mm:ssZ')
            count++;
            html += `<li class="conectionesAula ">
            <button class="mac btn btn-outline-primary" data-toggle="modal" data-target="#myModal">${equips[i].mac}</button>${equips[i].nom}<br><small>${moment(equips[i].data).format('YYYY-MM-DD')} -<span class="fecha" data="${haceCuanto}"> ${moment(haceCuanto).fromNow()}</small></span></li>`
        }
    }
    $("#aulaYConnectiones").html(`Classroom ${clicked} <br><div class="btn btn-xs btn-outline-light" id="classPcs">PC's: ${pintaNumeroOrdenadoresClicked(equips,clicked)} </div>-<div class="btn btn-xs btn-outline-light" id="connections"> ${count} connections</div>`)
    $("#listaConnectiones").html(html);
}

function pintaNumeroOrdenadoresClicked(equips, clicked) {
    let macsyaulas = devuelveMacsYAulas(equips)
    let count = 0
    macsyaulas.forEach(element => {
        if (element.aula == clicked) {
            count++;
        }
    });
    return count
}

function pintaOrdenadoresClicked(equips, clicked) {
    let macsyaulas = devuelveMacsYAulas(equips)
    let html = ""
    console.log(macsyaulas)
    macsyaulas.forEach(element => {
        if (element.aula == clicked) {
            html += `<button class="mac btn btn-outline-primary" data-toggle="modal" data-target="#myModal">${element.mac}</button>`
        }
    });
    $("#listaConnectiones").html(html);

}

function sortNumber(a, b) {
    return b - a;
}

function cuentaMacs(equips) {

    let macs = [];
    equips.forEach(element => {

        if (!(macs.includes(element.mac))) {
            macs.push(element.mac)
        }
    });
    console.log("Hay " + macs.length + "maquines");

    return macs.length;
    // Perform other work here ...
    // Set another completion function for the request above

}

function cuentaMesAnterior() {
    equips.forEach(element => {


        var date1 = new Date(element.data);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;
        /**
         * @param  {Date} today - Avui
         */
        var date2 = new Date(today);

        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        /**Calcula la diferencia, en dias, entre el dia actual y el dia que se publicó la oferta.
         * @param  {number} timeDiff Dias entre hoy y el dia de la publicacion.
         */
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

        //$("#datas").append(`<p id="${diffDays}"> MAC:${element.mac} data: ${element.data}`)
    });
    //$("#datas").append("hola")

}

function last30days(equips) {
    let esteMes = 0;
    equips.forEach(element => {


        var date1 = new Date(element);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;
        /**
         * @param  {Date} today - Avui
         */
        var date2 = new Date(today);

        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        /**Calcula la diferencia, en dias, entre el dia actual y el dia que se publicó la oferta.
         * @param  {number} timeDiff Dias entre hoy y el dia de la publicacion.
         */
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 30) {
            esteMes++;
        }
    });
    console.log("En los ultimos 30 dias se han conectado:" + esteMes)
    return esteMes;

}

function calculaDiferencia(diaConn) {
    var date1 = new Date(diaConn);
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = mm + '/' + dd + '/' + yyyy;
    /**
     * @param  {Date} today - Avui
     */
    var date2 = new Date(today);

    var timeDiff = Math.abs(date2.getTime() - date1.getTime()) / 36e5 / 24;
    /**Calcula la diferencia, en dias, entre el dia actual y el dia que se publicó la oferta.
     * @param  {number} timeDiff Dias entre hoy y el dia de la publicacion.
     */
    return timeDiff
}

function hideAll() {
    $(".cards").fadeOut()
    $("#listaDatos").fadeOut()
    $("#listaConnectiones").fadeOut();
    $("#tables").fadeOut();
    $("#divAulas").fadeOut()
    $(".connectionesAula").fadeOut()
    $("#charts").hide()
    $(".landingLogo").fadeOut()
}

function lastConnection(datas) {
    let max_dt = datas[datas.length - 1];
    max_dt = moment(max_dt).format('YYYY-MM-DDTHH:mm:ssZ')
    max_dt = moment(max_dt).fromNow()
    return max_dt;
}

function isHidden(el) {
    return (el.offsetParent === null)
}

function display(msg) {
    $("<p>").html(msg).appendTo(document.body);
}

function sortRams(a, b) {
    return (a * 10) - (b * 10);
}

function devuelveRamsUnicos(equips) {
    let rams = []
    for (i = 0; i < equips.length; i++) {
        //    console.log(maquines[i].aula)
        let elm = equips[i]
            // elm=elm.slice(0,-1);
            //elm=parseFloat(elm.replace(/,/g, '.')) //cambia las comas por puntos
        if (!(rams.includes(elm))) {
            rams.push(elm)
        }
    }
    return rams;
}

function devuelveMacYRam(equips) {

    let macs = [];
    let rams = [];
    equips.forEach(element => {

        if (!(macs.includes(element.mac))) {
            macs.push(element.mac)
            rams.push(element.ram)
        }
    });

    return rams;
    // Perform other work here ...
    // Set another completion function
}

function cuentaCuantosConRam(rams, ramsUnicos) {
    let cont = new Array(ramsUnicos.length).fill(0);
    rams.forEach(ram => {
        for (let i = 0; i < ramsUnicos.length; i++) {
            if (ram === ramsUnicos[i]) {
                cont[i]++;
            }
        }
    })
    return cont;
}

function cuentaCuantasConMac(mac, macs) {
    let count = 0;
    macs.forEach(element => {
        if (element === mac) count++;
    });
    return count;
}

function devuelveMacs(equips) {
    let macs = [];
    equips.forEach(element => {
        macs.push(element.mac)
    });
    return macs;
}

function pintaModalMac(equips, mac) {

    let con = [];
    equips.forEach(element => {
        if (element.mac === mac) {
            con.push(element)
        }
    });
    console.log(con)
    $(".modal-title").html("Mac: " + mac + "<br>" +
        "<small>" + "Name: " + con[con.length - 1].nom + "(" + con.length + ")" + "</small>");
    $(".modal-body").html(`
    <p> Ram: ${con[con.length-1].ram}</p>
    <p> Teacher's :</p>
    <p> Alone in the classroom: ${unicoOrdenador(equips,mac,con[con.length-1].aula)}</p>
    Classroom:  ${con[con.length-1].aula}  <p></p>
    <p>First register: ${moment(con[0].data).format('MMMM Do YYYY')} at ${moment(con[0].data).format('h:mm:ss a')}</p>
    <p>Last connection: ${moment(con[con.length-1].data).format('MMMM Do YYYY')} at ${moment(con[con.length-1].data).format('h:mm:ss a')}</p>

    `)
}

function devuelveMacsYAulas(equips) {


    //devuelve los macs unicos y la aula a la que pertenecen
    let macsMasAulas = [];
    let macsUnicos = [];
    equips.forEach(element => {

        if (!(macsUnicos.includes(element.mac))) {
            macsUnicos.push(element.mac)
            macsMasAulas.push({ aula: element.aula, mac: element.mac })
        }
    });

    return macsMasAulas;
}

function ordenadoresXAula(aulasUnicas, macsMasAulas) {
    let contAula = new Array(aulasUnicas.length).fill(0);
    macsMasAulas.forEach(element => {
        // console.log(element.aula);
        for (let i = 0; i < aulasUnicas.length; i++) {
            if (element.aula == aulasUnicas[i]) {
                contAula[i]++;
            }
        }
    });
    return contAula;
}

function PintaRamCard(equips) {
    let minRam = devuelveMinRam(equips);
    let maxRam = devuelveMaxRam(equips);
    $("#ram").html("");
    $("#ram").append(minRam + " / " + maxRam)
}

function pintaAulasYconn(equips, clicked) {
    aulas = DevuelveAulasUnicas(equips);
    pintaAulas(aulas)
    pintaClicked(equips, clicked)
    $(".connectionesAula").fadeIn()
    $("#listaConnectiones").fadeIn();
}

function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

//Dado una MAC y Equips devuelve true si es el unico ordenador
function unicoOrdenador(equips, mac, aula) {
    let aux = 0
    equips.forEach(element => {
        if (element.aula == aula && element.mac != mac) {
            aux = 1
        }
    });
    if (aux == 0) return true
    else return false;

}
/*SLIDER*/

///////////
/*
//Functionz

//Search by MAC ADDRESS.
function BuscaPorMac() {
    //console.log($("#name_search").val())
    $.ajax({
        method: "GET",
        url: "http://labs.iam.cat/api/api.php/records/equips",
        dataType: 'json'
    })
        .done(function (res) {
            let maquines = res.records;
            let html;
            for (let i = 0; i < maquines.length; i++) {
                //console.log(maquines[i].mac)
                if (maquines[i].mac === $("#name_search").val()) {
                    html += `<p>MAC:${maquines[i].mac} - ${maquines[i].data}</p>`
                } else if ($("#name_search").val() === "") {
                    html += `<br>MAC:${maquines[i].mac} - ${maquines[i].data}`
                }
            }
            $("#all").html(html);
        }).fail(function () {
            console.log("Error.")
        })
}
//return UNIQUE classsrooms.

//PRINTS: All registers for clicked classroom.
function OnClickClassroom() {
    //returns all registers for the clicked classroom
    $.ajax({
        method: "GET",
        url: "http://labs.iam.cat/api/api.php/records/equips",
        dataType: 'json'
    })
        .done(function (res) {
            let html, count = 0;
            let maquines = res.records;
            $("#aulas").on("click", "li", function () {
                html = '';
                count = 0;
                //console.log($(this).text())
                let clicked = $(this).text()
                for (let i = 0; i < maquines.length; i++) {
                    if (maquines[i].aula === clicked) {
                        console.log(maquines[i].aula)
                        count++
                        console.log(count)
                        html += `<p>MAC:${maquines[i].mac} - ${maquines[i].aula} - ${maquines[i].data}</p>`
                    }
                }


                $("#aulas").html(html);
                $(".total").html("<h5>" + count + "</h5>")

            })
        }).fail(function () {
            console.log("Error.")
        })
}
//RETURNS : Connections / Classroom

*/

/*function DevuelveArrayDatas(todo) {
    $.ajax({
            method: "GET",
            url: "http://labs.iam.cat/api/api.php/records/equips",
            dataType: 'json',
            async: false
        })
        .done(function(res) {
            let conection = res.records;

            conection.forEach(element => {
                todo.push(element.data)
            });
            // $("#all_data").append(html)
        })
        .fail(function() {
            console.log("Error.")
        })
}*/

/*

function DevuelveTodo(todo) {
    $.ajax({
            method: "GET",
            url: "http://labs.iam.cat/api/api.php/records/equips",
            dataType: 'json',

            async: false,
            beforeSend: function() {
                $(".load").fadeIn()
            }
        })
        .done(function(res) {
            let conection = res.records;

            conection.forEach(element => {
                todo.push(element)
            });
            $(".load").fadeOut()
                // $("#all_data").append(html)
        })
        .fail(function() {
            console.log("Error.")
        })
} */