$(document).ready(function() {
    let ultimaConection = lastConnection(equips);

    let ultimoMes = last30days(datas);
    let nMaquines = cuentaMacs(equips);
    let aulas = DevuelveAulasUnicas(equips);
    let rams = devuelveMacYRam(equips);
    let tiposRam = devuelveRamsUnicos(rams);
    let minRam = devuelveMinRam(equips);
    let maxRam = devuelveMaxRam(equips);
    let cuentaRams = cuentaCuantosConRam(rams, tiposRam)
    let macsYAulas = devuelveMacsYAulas(equips)
    let macsXAula = ordenadoresXAula(aulas, macsYAulas)

    //let connXRam= devuelveConnectionesXRam(tiposRam)
    //console.log(tiposRam)

    //Pintamos Aulas
    pintaAulas(aulas)

    /**********Cards*************/
    $("#nMaquines").html(nMaquines)
    $("#monthlyConnections").html(ultimoMes)
    $("#lastConnection").html(ultimaConection)
    $(".cards").fadeIn()
    $("#ram").html("");
    $("#ram").append(minRam + " / " + maxRam)

    /**********Events************/
    $('#listaAulas').on('click', '.aulas', function() {

        let html = "";
        let count = 0;
        let clicked = $(this).text()
        for (let i = 0; i < equips.length; i++) {
            if (equips[i].aula == clicked) {
                let fecha = equips[i].data
                fecha = moment(fecha).format('YYYY-MM-DDTHH:mm:ssZ')
                count++;
                html += `<li class="conectionesAula ">MAC:${equips[i].mac} - ${equips[i].aula} -<span class="fecha" data="${fecha}"> ${moment(fecha).fromNow()}</span></li>`
            }
        }
        $(".connectionesAula > .divTitle").html(`Classroom ${clicked} - ${count} connections`)
        hideAll()
        $("#listaConnectiones").html(html);
        $(".connectionesAula").fadeIn()
        $("#listaConnectiones").fadeIn();
        //  $(".total").html("<h5>" + count + "</h5>")



    })
    $('.nav-item').on('click', function() {

        $('.nav-item').removeClass("active")
        $(this).addClass("active")
    })
    $('#pieChartButton').on('click', function() {
        if ($('#pie-chart').is(':hidden')) {
            $('#pie-chart').fadeIn();
        } else {
            $('#pie-chart').fadeOut()
        }
    })
    $('#barChartButton').on('click', function() {
        if ($('#bar-chart').is(':hidden')) {
            $('#bar-chart').fadeIn();
        } else {
            $('#bar-chart').fadeOut()
        }
    })
    $('#horizontalBarChartButton').on('click', function() {
        console.log("hola")
        if ($('#bar-chart-horizontal').is(':hidden')) {
            $('#bar-chart-horizontal').show();
        } else {
            $('#bar-chart-horizontal').fadeOut()
        }
    })
    $('#botoTaules').on('click', function() {
        if ($('#tables').is(':hidden')) {
            $('#tables').fadeIn();
        } else {
            $('#tables').fadeOut()
        }
    })
    $("#dashboard").on("click", function() {
        if ($('.cards').is(':hidden')) {
            $('.cards').fadeIn();
        } else {
            $('.cards').fadeOut()
        }

    })
    $("#classroomsButton").on("click", function() {
        if ($('#divAulas').is(':hidden')) {
            $('#divAulas').fadeIn();
        } else {
            $('#divAulas').fadeOut()
            $(".connectionesAula").fadeOut()

            // $('.connectionesAula').hide()
        }
    })

    /**********Tables*************/
    $('#taulaCompleta').DataTable({
        data: equips,
        columns: [
            { data: 'data' },
            { data: 'mac' },
            { data: 'aula' },
            { data: 'ram' }
        ]
    });
    //Input Limit
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("fromLimit").setAttribute("max", today);

    /*Datepicker*/
    $(".date").datepicker({
        onSelect: function(dateText) {
            let picked = new Date(moment(this.value).format())
                //  console.log("it changed to " + picked)

            $(".fecha").each(function() {
                let dataCon = new Date(moment($(this).attr("data")).format())
                if (picked.getTime() < dataCon.getTime()) {
                    //        console.log(picked + "<" + dataCon)
                    $(this).parent().show()
                } else if (picked.getTime() < dataCon.getTime()) {
                    $(this).parent().hide();
                } else {
                    $(this).parent().fadeOut();
                }

            })

        }
    }).on("change", function() {
        display("Got change event from field");
    });

    /**********Tables*************/
    $('#taulaCompleta').DataTable({
        data: equips,
        columns: [
            { data: 'data' },
            { data: 'mac' },
            { data: 'aula' },
            { data: 'ram' }
        ]
    });
    /**********************     CHARTS      ***********************/
    // Bar chart
    let barChart = new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: aulas,
            datasets: [{
                label: "Connections",
                backgroundColor: ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
                ],

                data: ConnectionesXClase(aulas)
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            },

            legend: { display: false },
            title: {
                display: true,
                text: 'Connections for each classroom'
            }
        }
    });
    // Pie Chart
    new Chart(document.getElementById("pie-chart"), {
        type: 'pie',
        data: {
            labels: tiposRam,
            datasets: [{
                label: "Population (millions)",
                backgroundColor: ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                    '#80B300', '#809900'
                ],
                data: cuentaRams
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Quantity of computers with ram type'
            }
        }
    });

    //Horizontal Bar Chart
    new Chart(document.getElementById("bar-chart-horizontal"), {
        type: 'horizontalBar',
        data: {
            labels: aulas,
            backgroundColor: ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
            ],
            datasets: [{
                label: "Ordenadores por Aula ",
                data: macsXAula
            }]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Computers in each classroom'
            }
        }
    });


    //Slider
    /* $("p").each(function (index, element) {
         if (parseInt($(this).attr("id")) < 24 ) {
             console.log($(this).attr("id"))
 
         }
     });*/
    /*
        var date1 = new Date(oferta[i][4]);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    
    */
    /*
    //jQuery Slider
        var slider = document.getElementById("myRange");
        var output = document.getElementById("demo");
        output.innerHTML = slider.value;
            output.innerHTML = slider.value;
         slider.oninput = function () {
                output.innerHTML = this.value;
                console.log($("#myRange").val())
                sliderValue=this.value;
                val = this.value;
               // console.log(val)
            }
            slider.addEventListener('input', function (evt) {
                let val = this.value
            });
        
        
         */
});